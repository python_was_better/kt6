/** Container class to different classes, that makes the whole
 * set of classes one class formally.
 */
public class GraphTask {

   /** Main method. */
   public static void main (String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   public void run() {
      Graph g = new Graph("G");

      int[][] testMatrix = {
        {1, 0, 1},
        {1, 1, 0},
        {0, 1, 1}
      };
      g.createGraphFromAdjMatrix(testMatrix);

      int[][] adjMatrix = g.createAdjMatrix();
      g.transitiveClosureFloydWarshall(adjMatrix);
      g.createGraphFromAdjMatrix(adjMatrix);
   }

   static class Vertex {
      private final String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      public Vertex(String s, Vertex v, Arc e) {
         this.id = s;
         this.next = v;
         this.first = e;
      }

      public Vertex(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   /** Arc represents one arrow in the graph. Two-directional edges are
    * represented by two Arc objects (for both directions).
    */
   static class Arc {
      private final String id;
      private Vertex target;
      private Arc next;

      public Arc(String s, Vertex v, Arc a) {
         this.id = s;
         this.target = v;
         this.next = a;
      }

      public Arc(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }


   public static class Graph {
      private final String id;
      private Vertex first;

      public Graph(String s, Vertex v) {
         this.id = s;
         this.first = v;
      }

      public Graph(String s) {
         this(s, null);
      }

      @Override
      public String toString() {
         String nl = System.getProperty ("line.separator");
         StringBuilder sb = new StringBuilder(nl);
         sb.append (id);
         sb.append (nl);
         Vertex v = first;
         while (v != null) {
            sb.append (v.toString());
            sb.append (" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append (" ");
               sb.append (a.toString());
               sb.append (" (");
               sb.append (v.toString());
               sb.append ("->");
               sb.append (a.target.toString());
               sb.append (")");
               a = a.next;
            }
            sb.append (nl);
            v = v.next;
         }
         return sb.toString();
      }

      public Vertex createVertex(String vid) {
         Vertex res = new Vertex (vid);
         res.next = first;
         first = res;
         return res;
      }

      public void createArc(String aid, Vertex from, Vertex to) {
         Arc res = new Arc (aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       * @param n number of vertices added to this graph
       */
      public void createRandomTree (int n) {
         if (n <= 0) return;
         Vertex[] varray = new Vertex [n];
         for (int i = 0; i < n; i++) {
            varray [i] = createVertex ("v" + (n - i));
            if (i > 0) {
               int vnr = (int)(Math.random()*i);
               createArc ("a" + varray [vnr].toString() + "_"
                 + varray [i].toString(), varray [vnr], varray [i]);
               createArc ("a" + varray [i].toString() + "_"
                 + varray [vnr].toString(), varray [i], varray [vnr]);
            }
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       * @return adjacency matrix
       */
      public int[][] createAdjMatrix() {
         int info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int [info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res [i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       * @param n number of vertices
       * @param m number of edges
       */
      public void createRandomSimpleGraph (int n, int m) {
         if (n <= 0)
            return;
         if (m < n-1 || m > n*(n-1)/2)
            throw new IllegalArgumentException
              ("Impossible number of edges: " + m);
         first = null;
         createRandomTree (n);       // n-1 edges created here
         Vertex[] vert = new Vertex [n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int)(Math.random()*n);  // random source
            int j = (int)(Math.random()*n);  // random target
            if (i==j)
               continue;  // no loops
            if (connected [i][j] != 0 || connected [j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert [i];
            Vertex vj = vert [j];
            createArc ("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected [i][j] = 1;
            createArc ("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected [j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Given an adjacency matrix of an unweighted directed graph,
       * find the shortest distances between every pair of vertices
       * by using the Floyd Warshall's algorithm.
       * Result of this function is a transitive closure of an input matrix.
       * @param matrix an adjacency matrix of a graph
       *               (Every "1" in a matrix indicates a pair between two vertices)
       */
      public void transitiveClosureFloydWarshall(int[][] matrix) {
         for (int k = 0; k < matrix.length; k++) {
            for (int i = 0; i < matrix.length; i++) {
               if (matrix[i][k] == 1) {
                  for (int j = 0; j < matrix.length; j++) {
                     if (matrix[k][j] == 1) {
                        matrix[i][j] = 1;
                     }
                  }
               }
            }
         }
      }

      /**
       * Take an adjacency matrix and override the existing graph according
       * to the structure of the input matrix.
       * @param matrix an adjacency matrix of a graph
       *               (Every "1" in a matrix indicates a pair between two vertices)
       */
      public void createGraphFromAdjMatrix(int[][] matrix) {
         first = null;
         Vertex[] vertices = new Vertex[matrix.length];

         for (int i = 0; i < matrix.length; i++) {
            vertices[i] = createVertex(String.valueOf(matrix.length - 1 - i));
         }

         for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
               if (matrix[i][j] == 1) {
                  createArc("", vertices[matrix.length - 1 - i], vertices[matrix.length - 1 - j]);
               }
            }
         }
      }
   }
}

