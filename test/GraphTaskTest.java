import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static java.lang.System.currentTimeMillis;
import static org.assertj.core.api.Assertions.assertThat;

/** Testklass.
 * @author jaanus
 */
public class GraphTaskTest {
   private static Long startingPoint;
   private static GraphTask.Graph graph;

   @BeforeEach
   public void setUp() {
      graph = new GraphTask.Graph("G");
      startingPoint = currentTimeMillis();
   }

   @Test
   @DisplayName(
     "The matrix of the graph is following: "
       + "0, 1, 1"
       + "0, 0, 1"
       + "1, 0, 0"
   )
   public void testFloydWarshallAlgorithmTransitiveClosure_example1() {
      graph.createGraphFromAdjMatrix(new int[][]{
        {0,1,1},{0,0,1},{1,0,0}
      });

      int[][] adjMatrix = graph.createAdjMatrix();
      graph.transitiveClosureFloydWarshall(adjMatrix);

      assertThat(adjMatrix).isEqualTo(new int[][]{
        {1,1,1},{1,1,1},{1,1,1}
      });
   }

   @Test
   @DisplayName(
     "The matrix of the graph is following: "
       + "1, 1, 0, 1"
       + "0, 0, 0, 0"
       + "0, 1, 0, 1"
       + "0, 1, 0, 1"
   )
   public void testFloydWarshallAlgorithmTransitiveClosure_example2() {
      graph.createGraphFromAdjMatrix(new int[][]{
        {1,1,0,1},{0,0,0,0},{0,1,0,1},{0,1,0,1}
      });

      int[][] adjMatrix = graph.createAdjMatrix();
      graph.transitiveClosureFloydWarshall(adjMatrix);

      assertThat(adjMatrix).isEqualTo(new int[][]{
        {1,1,0,1},{0,0,0,0},{0,1,0,1},{0,1,0,1}
      });
   }

   @Test
   @DisplayName(
     "The matrix of the graph is following: "
       + "0, 1, 1, 0"
       + "0, 0, 1, 0"
       + "1, 0, 0, 1"
       + "0, 0, 0, 0"
   )
   public void testFloydWarshallAlgorithmTransitiveClosure_example3() {
      graph.createGraphFromAdjMatrix(new int[][]{
        {0,1,1,0},{0,0,1,0},{1,0,0,1},{0,0,0,0}
      });

      int[][] adjMatrix = graph.createAdjMatrix();
      graph.transitiveClosureFloydWarshall(adjMatrix);

      assertThat(adjMatrix).isEqualTo(new int[][]{
        {1,1,1,1},{1,1,1,1},{1,1,1,1},{0,0,0,0}
      });
   }

   @Test
   @DisplayName(
     "The matrix of the graph is following: "
       + "1, 0, 1, 0, 0"
       + "1, 1, 0, 0, 0"
       + "0, 1, 1, 0, 0"
       + "0, 0, 1, 1, 1"
       + "0, 0, 0, 0, 1"
   )
   public void testFloydWarshallAlgorithmTransitiveClosure_example4() {
      graph.createGraphFromAdjMatrix(new int[][]{
        {1,0,1,0,0},{1,1,0,0,0},{0,1,1,0,0},{0,0,1,1,1},{0,0,0,0,1}
      });

      int[][] adjMatrix = graph.createAdjMatrix();
      graph.transitiveClosureFloydWarshall(adjMatrix);

      assertThat(adjMatrix).isEqualTo(new int[][]{
        {1,1,1,0,0},{1,1,1,0,0},{1,1,1,0,0},{1,1,1,1,1},{0,0,0,0,1}
      });
   }

   @RepeatedTest(10)
   public void testMatrixToGraphTransformation() {
      graph.createRandomSimpleGraph(10, 10);
      int[][] originalMatrix = graph.createAdjMatrix();

      graph.createGraphFromAdjMatrix(originalMatrix);
      int[][] transformedGraphMatrix = graph.createAdjMatrix();

      assertThat(originalMatrix).isEqualTo(transformedGraphMatrix);
   }

   @RepeatedTest(25)
   public void testCase5000Vertices() {
      graph.createRandomSimpleGraph(5000, 5000);
      int[][] adjMatrix = graph.createAdjMatrix();
      graph.transitiveClosureFloydWarshall(adjMatrix);
      graph.createGraphFromAdjMatrix(adjMatrix);

      assertThat(currentTimeMillis() - startingPoint).isLessThan(2000L);
   }

   @ParameterizedTest
   @ValueSource(ints = {500, 1000, 2000, 4000, 8000})
   public void speedTest(int n) {
      graph.createRandomSimpleGraph(n, n);
      System.out.println("Graph creation took "
        + (currentTimeMillis() - startingPoint) + " milliseconds");

      long matrixCreationStart = currentTimeMillis();
      int[][] adjMatrix = graph.createAdjMatrix();
      System.out.println("Matrix creation took "
        + (currentTimeMillis() - matrixCreationStart) + " milliseconds");

      long algorithmStart = currentTimeMillis();
      graph.transitiveClosureFloydWarshall(adjMatrix);
      System.out.println("Algorithm execution took "
        + (currentTimeMillis() - algorithmStart) + " milliseconds");

      long transformationStart = currentTimeMillis();
      graph.createGraphFromAdjMatrix(adjMatrix);
      System.out.println("Graph transformation took "
        + (currentTimeMillis() - transformationStart) + " milliseconds");

      System.out.println("Overall execution time is "
        + (currentTimeMillis() - startingPoint) + " milliseconds");
   }
}